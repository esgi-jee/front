import React, {useEffect} from 'react';
import { useForm } from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {getOneVolunteerByEmail} from "../actions/API/volunteer.action";
import AuthService from "../components/Auth/AuthService";
import CardMission from "../components/pages/CardMission";
import {isEmpty} from "../components/utils/Utils";
import {useState} from "react";
import axios from "axios";

const Profil = () => {
    const { register  } = useForm();


    const dispatch = useDispatch();
    const [missionValue,setMissionValue] = useState([])
    useEffect( () => {
        axios
            .get(`https://api-hesspa.azurewebsites.net/missions `, { headers: AuthService.authHeader() })
            .then((res) => {
                setMissionValue(res.data)

            })
            .catch((err) => console.log(err));
    },[])



    useEffect( () => {
         dispatch(getOneVolunteerByEmail(AuthService.getEmailUserConnected()))
    },)
    const user = useSelector((state) => state.volunteerReducer )

    return (
        <div className="view--profile">
            <div className="profil-header">
                <img src="/assets/images/image-profile.svg" alt="" className="image-profile"/>
                <div className="profile-title">Profile</div>
            </div>
            <form action="" className="form-body">
                <div className="container-body">
                    <div className="container-form">
                        <div className="title-form">Name</div>
                        <input {...register("name", { required: "Please enter your  name." })} type="text" defaultValue={user.name} placeholder="name" className="form-input"/>
                    </div>
                    <div className="container-form">
                        <div className="title-form">Job</div>
                        <input {...register("job", { required: "Please enter your job." })} type="text" defaultValue={user.job} placeholder="job" className="form-input"/>
                    </div>
                </div>
                <div className="container-body">
                    <div className="container-form">
                        <div className="title-form">Mail</div>
                        <input {...register("mail", { required: "Please enter your mail." })} type="text" defaultValue={user.mail} placeholder="mail" className="form-input"/>
                    </div>
                    <div className="container-form">
                        <div className="title-form">Password</div>
                        <input {...register("password", { required: "Please enter your password." })} type="password" defaultValue={"password"} desibled placeholder="password" className="form-input"/>
                    </div>
                </div>
                <div className="button-send">Enregistrer</div>
            </form>
        </div>
    );
};

export default Profil;