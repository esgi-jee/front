
import CardPets from "../components/pages/CardPets";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {API_URL, GET_ANIMAL, getAnimals} from "../actions/API/animals.action";
import {isEmpty} from "../components/utils/Utils";
import axios from "axios";
import AuthService from "../components/Auth/AuthService";

const Pets = () => {

    const history = useHistory();

    //constante d'appel
    const dispatch = useDispatch();

    //appel a l'actions
    useEffect( () => {
        axios
            .get(`${API_URL}/animals`, { headers: AuthService.authHeader() })
            .then((res) => {
                setAnimalsValue(res.data);

            })
            .catch((err) => console.log(err));
    },[])

    //recuperation des data
    const [animalsValue,setAnimalsValue] =  useState([])

    console.log(animalsValue)


    const onClickPageDescriptionPets = (name) => {
        history.push({
            pathname : `/pet/${name}`
        })
    }

    return (
        <div className="view--pets">
            <div className="title-header">ALL Pets</div>
            <div className="container-pets">
                {
                    !isEmpty(animalsValue)&&
                    animalsValue.map((value,index) =>
                        <div key={index} className="pets-card" onClick={() => onClickPageDescriptionPets(value.id)}>
                            <CardPets data={value} link={value.pictureUrl}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Pets;