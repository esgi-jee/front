import React, {useState} from 'react';

import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";

import axios from "axios";
import AuthService from "../components/Auth/AuthService";
import {isEmpty} from "../components/utils/Utils";
import CardMission from "../components/pages/CardMission";

const Mission = () => {

    const dispatch = useDispatch();
    const [missionValue,setMissionValue] = useState([])
    useEffect( () => {
        axios
            .get(`https://api-hesspa.azurewebsites.net/missions `, { headers: AuthService.authHeader() })
            .then((res) => {
                setMissionValue(res.data)

            })
            .catch((err) => console.log(err));
    },[])

    console.log(missionValue)

    return (
        <div className="view--mission">
            <div className="title-mission">Missions Disponible</div>
            <div className="container-body">
                {
                    !isEmpty(missionValue) &&
                        missionValue.map((value,index) =>
                            <CardMission key={index} titleMission={value.description} url={value.animals[index].href} dateStart={value.startDate} dateStop={value.endDate}/>
                        )
                }
            </div>
        </div>
    );
};

export default Mission;