
import React, {useEffect, useState} from "react"
import {Link} from "react-router-dom";
import AuthService from "../components/Auth/AuthService";
import {isEmpty} from "../components/utils/Utils";


const Home = () => {
    //TODO : faire csss
    const [isConnected, setIsConnected] = useState(AuthService.getCurrentUser());
    return(
        <div className='view--home'>
            <div className="left-part">
                <div className="title-left-part">Don't forget your animal !</div>
                <div className="subtitle-left-part">Adopt your pet</div>
                {
                    !isEmpty(isConnected) &&
                    <Link to="/all-pets" style={{textDecoration:'none', color:'#fff'}}>
                        <div className="button-left-part">All Pets</div>
                    </Link>
                }
                {
                    isEmpty(isConnected) &&
                    <Link to="/login" style={{textDecoration:'none', color:'#fff'}}>
                        <div className="button-left-part">Get Started</div>
                    </Link>
                }
            </div>
            <img src="/assets/images/image-home.png" alt="animal image" className="right-part"/>
        </div>
    )
}

export default Home;