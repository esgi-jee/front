import axios from "axios";
import AuthService from "../../components/Auth/AuthService";



export const API_URL = "https://api-hesspa.azurewebsites.net"
export const GET_ANIMAL = "GET_ANIMAL";
export const GET_ANIMAL_BY_ID = "GET_ANIMAL_BY_ID";
export const ADD_ANIMAL = "ADD_ANIMAL";
export const DELETE_ANIMAL = "DELETE_ANIMAL";

export const getAnimals = () => {
    return (dispatch) => {
        return axios
            .get(`${API_URL}/animals`, { headers: AuthService.authHeader() })
            .then((res) => {
                dispatch({ type: GET_ANIMAL, payload: res.data });

            })
            .catch((err) => console.log(err));
    };
};

export const getOneAnimalById = (animalId) => {
    return (dispatch) => {
        return axios
            .get(`${API_URL}/animals?id=${animalId}`,{ headers:  AuthService.authHeader() })
            .then((res) => {
                dispatch({ type: GET_ANIMAL_BY_ID, payload: res.data });
            })
            .catch((err) => console.log(err));
    };
};

export const addAnimal = (data) => {
    return (dispatch) => {
        return axios
            .post(`${API_URL}/animals`, data, { headers:  AuthService.authHeader() })
            .then(() => {
                dispatch({ type: ADD_ANIMAL, payload: data });
            })
            .catch((err) => console.log(err));
    };
};



export const deleteAnimal = (animalId) => {
    return (dispatch) => {
        return axios({
            method: "delete",
            url: `${API_URL}/animals/delete/${animalId}`,
            headers:  AuthService.authHeader()

        })
            .then(() => {
                dispatch({ type: DELETE_ANIMAL, payload: { animalId } });
            })
            .catch((err) => console.log(err));
    };
};