import axios from "axios";
import qs from 'querystring'

export const API_URL = "https://api-hesspa.azurewebsites.net/"
export const LOGIN_USER = "LOGIN_USER";
export const REGISTER_USER = "REGISTER_USER";


export const login = (data) => {
    return (dispatch) => {
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            data: qs.stringify(data),
            url: API_URL + '/login',
        };
        axios(options).then(response => {
            if (response.data.accessToken) {
                dispatch({ type: LOGIN_USER, payload: response.data.accessToken });
                localStorage.setItem("user", JSON.stringify(response.data.accessToken));
                if(response.status === 200) {
                    window.location.replace('/');
                }
            }
            return response.data;
        });
    };
}


export const register_user = (data) => {
    return (dispatch) => {
        return axios
            .post(API_URL + "/volunteers", data)
            .then((res) => {
                if(res.status === 201) {
                    window.location.replace('/login');
                }
                dispatch({ type: REGISTER_USER, payload: res });
            })
            .catch((err) => console.log(err));
    };
}





