import AuthService from "../../components/Auth/AuthService";
import axios from "axios";
import {ADD_ANIMAL, DELETE_ANIMAL, GET_ANIMAL} from "./animals.action";



export const API_URL = "https://api-hesspa.azurewebsites.net/"
export const GET_MISSION = "GET_MISSION";
export const ASSIGN_MISSION = "ASSIGN_MISSION";
export const DELETE_MISSION = "DELETE_MISSION";


export const getMission = () => {
    return (dispatch) => {
        return axios
            .get(`${API_URL}/missions`, { headers: AuthService.authHeader() })
            .then((res) => {
                dispatch({ type: GET_MISSION, payload: res.data });

            })
            .catch((err) => console.log(err));
    };
};


export const assignMission = (data) => {
    return (dispatch) => {
        return axios
            .post(`${API_URL}/missions`, data, { headers:  AuthService.authHeader() })
            .then(() => {
                dispatch({ type: ASSIGN_MISSION, payload: data });
            })
            .catch((err) => console.log(err));
    };
};


export const deleteAnimal = (animalId) => {
    return (dispatch) => {
        return axios({
            method: "delete",
            url: `${API_URL}/missions`,
            headers:  AuthService.authHeader()

        })
            .then(() => {
                dispatch({ type: DELETE_MISSION, payload: { animalId } });
            })
            .catch((err) => console.log(err));
    };
};
