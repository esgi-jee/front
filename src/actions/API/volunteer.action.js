import axios from "axios";
import AuthService from "../../components/Auth/AuthService";



export const API_URL = "https://api-hesspa.azurewebsites.net/"
export const GET_VOLUNTEER = "GET_VOLUNTEER";
export const GET_VOLUNTEER_BY_ID = "GET_VOLUNTEER_BY_ID";
export const ADD_VOLUNTEER = "ADD_VOLUNTEER";
export const DELETE_VOLUNTEER = "DELETE_VOLUNTEER";

export const getVolunteers = () => {
    return (dispatch) => {
        return axios
            .get(`${API_URL}/volunteers`, { headers: AuthService.authHeader() })
            .then((res) => {
                dispatch({ type: GET_VOLUNTEER, payload: res.data });

            })
            .catch((err) => console.log(err));
    };
};

export const getOneVolunteerByEmail = (volunteerEmail) => {
    return (dispatch) => {
        return axios
            .get(`${API_URL}/volunteers?mail=${volunteerEmail}`,{ headers:  AuthService.authHeader() })
            .then((res) => {
                dispatch({ type: GET_VOLUNTEER_BY_ID, payload: res.data });
            })
            .catch((err) => console.log(err));
    };
};

export const addVOLUNTEER = (data) => {
    return (dispatch) => {
        return axios
            .post(`${API_URL}/volunteers`, data, { headers:  AuthService.authHeader() })
            .then(() => {
                dispatch({ type: ADD_VOLUNTEER, payload: data });
            })
            .catch((err) => console.log(err));
    };
};



export const deleteVOLUNTEER = (volunteerId) => {
    return (dispatch) => {
        return axios({
            method: "delete",
            url: `${API_URL}/volunteers/delete/${volunteerId}`,
            headers:  AuthService.authHeader()

        })
            .then(() => {
                dispatch({ type: DELETE_VOLUNTEER, payload: { volunteerId } });
            })
            .catch((err) => console.log(err));
    };
};