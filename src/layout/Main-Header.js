import React, {useEffect, useState} from 'react';
import {Link, useLocation} from "react-router-dom";
import AuthService from "../components/Auth/AuthService";
import {isEmpty} from "../components/utils/Utils";


const MainHeader = () => {

    const [isConnected, setIsConnected] = useState(AuthService.getCurrentUser());
    const location = useLocation()
    useEffect( () => {
         setIsConnected(AuthService.getCurrentUser())
    }, [location.key])

    return (
        <>

            <div className="main-header">
                <Link to="/" style={{textDecoration:'none', color:'#1D266E'}}>
                    <div className="link">
                        <img src="/assets/logo/logo.png" alt="logo" className="logo-header"/>
                    </div>
                </Link>
                <div className="link-container">
                    <Link to="/" style={{textDecoration:'none', color:'#1D266E'}}>
                        <div className="link">
                            <div className="title-link">Home</div>
                        </div>
                    </Link>
                    <Link to="/all-pets" style={{textDecoration:'none', color:'#1D266E'}}>
                        <div className="link">
                            <div className="title-link">All Pets</div>
                        </div>
                    </Link>
                    {
                        !isEmpty(isConnected) &&
                        <Link to="/mission" style={{textDecoration:'none', color:'#1D266E'}}>
                            <div className="link">
                                <div className="title-link">Missions</div>
                            </div>
                        </Link>
                    }
                    {/*

                        <Link to="/contact" style={{textDecoration:'none', color:'#1D266E'}}>
                            <div className="link">
                                <div className="title-link">Contact us</div>
                            </div>
                        </Link>
                    */}
                </div>
                <div className="auth-container">
                    {
                        isEmpty(isConnected) &&
                        <Link to="/login" style={{textDecoration:'none', color:'#FF7143'}}>
                            <div className="button-auth">Login</div>
                        </Link>
                    }
                    {
                        !isEmpty(isConnected) &&
                        <Link to="/profil" style={{textDecoration:'none', color:'#fff'}}>
                            <img className="profile-img" src="/assets/logo/logo-profil.svg" alt=""/>
                        </Link>
                    }
                </div>
            </div>
        </>
    );
};

export default MainHeader;