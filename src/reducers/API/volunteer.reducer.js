import {
    GET_VOLUNTEER,
    GET_VOLUNTEER_BY_ID,
    ADD_VOLUNTEER,
    DELETE_VOLUNTEER
} from "../../actions/API/volunteer.action";

const initialState = {};

export default function volunteerReducer(state = initialState, action) {
    switch (action.type) {
        case GET_VOLUNTEER:
            return action.payload;
        case GET_VOLUNTEER_BY_ID:
            return action.payload;
        case ADD_VOLUNTEER:
            return [action.payload, ...state];
        case DELETE_VOLUNTEER:
            return state.filter((VOLUNTEER) => VOLUNTEER.id !== action.payload.volunteerId);
        default:
            return state;
    }
}