import {
    GET_ANIMAL,
    GET_ANIMAL_BY_ID,
    ADD_ANIMAL,
    DELETE_ANIMAL
} from "../../actions/API/animals.action";

const initialState = {};

export default function animalReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ANIMAL:
            return action.payload;
        case GET_ANIMAL_BY_ID:
            return action.payload;
        case ADD_ANIMAL:
            return [action.payload, ...state];
        case DELETE_ANIMAL:
            return state.filter((ANIMAL) => ANIMAL.id !== action.payload.animalId);
        default:
            return state;
    }
}