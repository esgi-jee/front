import {
    GET_MISSION,
    ASSIGN_MISSION,
    DELETE_MISSION
} from "../../actions/API/missions.action"

const initialState = {};

export default function missionReducer(state = initialState, action) {
    switch (action.type) {
        case GET_MISSION:
            return action.payload;
        case ASSIGN_MISSION:
            return [action.payload, ...state];
        case DELETE_MISSION:
            return state.filter((ANIMAL) => ANIMAL.id !== action.payload.animalId);
        default:
            return state;
    }
}