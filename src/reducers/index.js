import { combineReducers } from "redux";
import authReducer from "./API/auth.reducer";
import volunteerReducer from "./API/volunteer.reducer";
import animalReducer from "./API/animal.reducer";
import missionReducer from "./API/mission.reducer";

export default combineReducers({
    authReducer,
    volunteerReducer,
    animalReducer,
    missionReducer
});
