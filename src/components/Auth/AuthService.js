
import jwt_decode from "jwt-decode";

class  AuthService {

    logout = () => {
        localStorage.removeItem("user");
    }

    getCurrentUser = () => {
          return JSON.parse(localStorage.getItem('user'));
    }

    authHeader = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user) {
            return {
                Authorization: 'Bearer ' + user,
            };
        } else {
            return {};
        }
    }

    getEmailUserConnected = () => {
        let token = this.getCurrentUser();
        if(token){
            let decoded = jwt_decode(token);
            return decoded.sub;
        }else {
            return null
        }
    }

};

export default new AuthService();