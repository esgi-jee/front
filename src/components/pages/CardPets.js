import React from 'react';

const CardPets = ({link, data = {}}) => {
    return (
        <div className="component--card-pets">
            <img src={link} alt="image pet" className="header-card"/>
            <div className="body-card">
                <div className="info-container">
                    <div className="title-info"> Name: <span>{data.name}</span> </div>
                    <div className="title-info"> species: <span>{data.species}</span> </div>
                </div>
                <div className="info-container">
                    <div className="title-info"> description: <span>{data.description}</span> </div>
                </div>
            </div>
        </div>
    );
};

export default CardPets;