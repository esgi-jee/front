import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import { getOneAnimalById} from "../../actions/API/animals.action";
import {useParams} from "react-router-dom";

const PetsDescription = () => {

    const dispatch = useDispatch();

    const {id} = useParams();

    useEffect( () => {
         dispatch(getOneAnimalById(id))
    },[])
    const animal = useSelector((state) => state.animalReducer )


    return (
        <div className="component--pets-description">
            <div className="title-pets">{animal.name}</div>
            <div className="container-pets-container">
                <div className="left-part">
                    <img src={animal.pictureUrl} alt="" className="image-pets"/>
                </div>
                <div className="right-part">
                    <div className="container-info">
                        <div className="container-title-info">
                            <div className="title-info">Name</div>
                            <input type="text" desibled defaultValue={animal.name} />
                        </div>
                        <div className="container-title-info">
                            <div className="title-info">Type d'animal</div>
                            <input type="text" desibled defaultValue={animal.species} />
                        </div>
                    </div>
                    <div className="container-info">
                        <div className="container-title-info">
                            <div className="title-info">Description</div>
                            <input type="text" desibled defaultValue={animal.description} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PetsDescription;