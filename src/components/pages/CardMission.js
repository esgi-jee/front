import React, {useState} from 'react';
import {useEffect} from "react";
import axios from "axios";
import AuthService from "../Auth/AuthService";

const CardMission = ({titleMission, url, dateStart,dateStop}) => {

    const [aniamlValue,setAnimalValue] = useState([])
    useEffect( () => {
        axios
            .get(`${url} `, { headers: AuthService.authHeader() })
            .then((res) => {
                setAnimalValue(res.data)

            })
            .catch((err) => console.log(err));
    },[])

    console.log(aniamlValue)

    const [userMission, setUserMission] = useState(false)

    const handleAssignMissionToUser = () =>{
        setUserMission(true)
    }

    const handleDeleteMissionToUser = () =>{
        setUserMission(false)
    }

    return (
        <div className="component--card-mission">
            <div className="header-card-mission">
                <div className="title-header">Mission : {titleMission}</div>
            </div>
            <div className="body-container">
                <div className="left-part">
                    <img src={aniamlValue.pictureUrl} alt="image animal"/>
                </div>
                <div className="right-part">
                    <div className="title-date">Name : {aniamlValue.name}</div>
                    <div className="title-date">Debut : {dateStart}</div>
                    <div className="title-date">Fin : {dateStop}</div>
                </div>
            </div>
        </div>
    );
};

export default CardMission;